const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;
require("dotenv").config()

mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@cluster0.qulgpmi.mongodb.net/s35-activity?retryWrites=true&w=majority`,
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", () => console.log("Connection error"));
db.once("open", () => console.log("Connected to MongoDB!"));

const user_schema = new mongoose.Schema({
	name: String,
	password: String
});

const User = mongoose.model("User", user_schema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post('/signup', (request, response) => {
	User.findOne({name: request.body.name, password: request.body.password}).then((result, error) => {
		if(result !== null && result.name == request.body.name && result.password == request.body.password){
			return response.send("Duplicate User found!")
		} else {
			let new_user = new User({
				name: request.body.name,
				password: request.body.password
			})

			new_user.save().then((created_task, error) => {
				if(error){
					return console.log(error)
				} else{
					return response.status(201).send("New User created!")
				}
			})
		}
	})
});

app.get("/users", (request, response) => {
	User.find({}).then((result, error) => {
		if(error){
			return console.log(error)
		}

		return response.status(200).send(result)
	})
})
 

app.listen(port, () => console.log(`The server is running at localhost:${port}`));
